package com.shahzad.movielist;

import android.app.Application;

import com.shahzad.movielist.navigator.Navigator;
import com.shahzad.movielist.network.rest.MovieService;


public class MovieApplication extends Application {
    private MovieService movieService;
    private Navigator navigator;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeDependencies();
    }

    private void initializeDependencies() {
        movieService = new MovieService();
        navigator = new Navigator();
    }

    public MovieService getMovieService() {
        return movieService;
    }

    public Navigator getNavigator() {
        return navigator;
    }
}
