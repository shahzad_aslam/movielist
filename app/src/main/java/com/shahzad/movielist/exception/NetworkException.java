package com.shahzad.movielist.exception;

public class NetworkException extends Exception {
    public NetworkException(int code) {
        super(Integer.toString(code));
    }

    public NetworkException(Throwable throwable) {
        super(throwable);
    }
}
