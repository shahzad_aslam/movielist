package com.shahzad.movielist.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpcomingMovies {
    @SerializedName("results")
    private List<Movie> movieList;
    @SerializedName("page")
    private int page;
    @SerializedName("total_results")
    private int totalResults;
    @SerializedName("total_pages")
    private int totalPages;
    @SerializedName("dates")
    private Dates dates;

    public List<Movie> getMovieList() {
        return movieList;
    }

    public int getPage() {
        return page;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public Dates getDates() {
        return dates;
    }


    public static class Dates {
        @SerializedName("maximum")
        private String maximum;
        @SerializedName("minimum")
        private String minimum;

        public String getMaximum() {
            return maximum;
        }

        public String getMinimum() {
            return minimum;
        }
    }
}


