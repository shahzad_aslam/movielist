package com.shahzad.movielist.navigator;

import android.content.Context;

import com.shahzad.movielist.view.activity.MovieDetailsActivity;

public class Navigator {
    public void navigateToMovieDetailsActivity(final Context context, int movieId) {
        if (context != null) {
            context.startActivity(MovieDetailsActivity.getCallerIntent(context, movieId));
        }
    }
}