package com.shahzad.movielist.network.rest;

import com.shahzad.movielist.model.MovieDetails;

public interface MovieDetailsListener extends MovieServiceListener{
    void onResultReceived(MovieDetails movieDetails);
}