package com.shahzad.movielist.network.rest;

import com.shahzad.movielist.exception.NetworkException;
import com.shahzad.movielist.model.MovieDetails;
import com.shahzad.movielist.model.UpcomingMovies;
import com.shahzad.movielist.network.rest.api.MovieServiceApi;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieService {

    static final String BASE_URL = "https://api.themoviedb.org";
    private static final String API_KEY = "5f09872bea39477b3975da2adb18675a";
    private MovieServiceApi serviceApi;

    public MovieService() {
        initService();
    }

    private void initService() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Retrofit service = retrofit;
        serviceApi = service.create(MovieServiceApi.class);
    }

    public void getUpComingMovies(int pageNumber, final UpComingMoviesListener listener) {
        serviceApi.upComingMovies(API_KEY, pageNumber).enqueue(new Callback<UpcomingMovies>() {
            @Override
            public void onResponse(Call<UpcomingMovies> call, Response<UpcomingMovies> response) {
                if (response.isSuccessful()) {
                    listener.onResultReceived(response.body());
                } else {
                    listener.onErrorReceived(new NetworkException(response.code()));
                }
            }

            @Override
            public void onFailure(Call<UpcomingMovies> call, Throwable throwable) {
                listener.onErrorReceived(throwable);
            }
        });
    }

    public void getMovieDetails(int movieId, final MovieDetailsListener listener) {
        serviceApi.movieDetails(movieId, API_KEY).enqueue(new Callback<MovieDetails>() {
            @Override
            public void onResponse(Call<MovieDetails> call, Response<MovieDetails> response) {
                if (response.isSuccessful()) {
                    listener.onResultReceived(response.body());
                } else {
                    listener.onErrorReceived(new NetworkException(response.code()));
                }
            }

            @Override
            public void onFailure(Call<MovieDetails> call, Throwable throwable) {
                listener.onErrorReceived(throwable);
            }
        });
    }
}

