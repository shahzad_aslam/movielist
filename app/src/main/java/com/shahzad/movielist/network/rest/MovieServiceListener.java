package com.shahzad.movielist.network.rest;

public interface MovieServiceListener {
    void onErrorReceived(final Throwable throwable);
}