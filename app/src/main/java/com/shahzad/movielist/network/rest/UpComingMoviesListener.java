package com.shahzad.movielist.network.rest;

import com.shahzad.movielist.model.UpcomingMovies;

public interface UpComingMoviesListener extends MovieServiceListener {
    void onResultReceived(UpcomingMovies body);
}
