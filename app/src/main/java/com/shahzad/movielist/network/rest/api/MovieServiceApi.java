package com.shahzad.movielist.network.rest.api;

import com.shahzad.movielist.model.MovieDetails;
import com.shahzad.movielist.model.UpcomingMovies;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieServiceApi {
    @GET("/3/movie/upcoming")
    Call<UpcomingMovies> upComingMovies(@Query("api_key") String apiKey, @Query("page") int pageIndex);

    @GET("/3/movie/{movie_id}")
    Call<MovieDetails> movieDetails(@Path("movie_id") int movieId, @Query("api_key") String apiKey);
}
