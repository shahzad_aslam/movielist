package com.shahzad.movielist.presenter;

public abstract class BaseFragmentPresenter<V> {
    protected V view;

    public abstract void setView(V view);

    public abstract void onViewCreated();
}
