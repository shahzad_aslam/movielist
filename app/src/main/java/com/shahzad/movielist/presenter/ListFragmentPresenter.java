package com.shahzad.movielist.presenter;

import com.shahzad.movielist.model.UpcomingMovies;
import com.shahzad.movielist.network.rest.MovieService;
import com.shahzad.movielist.network.rest.UpComingMoviesListener;
import com.shahzad.movielist.view.IMovieListView;

public class ListFragmentPresenter extends BaseFragmentPresenter<IMovieListView> implements UpComingMoviesListener {
    private final MovieService movieService;
    private int currentPage = 1;
    private boolean initialListLoaded;
    private int totalPages = 0;

    public ListFragmentPresenter(MovieService movieService) {
        this.movieService = movieService;
    }

    @Override
    public void setView(IMovieListView view) {
        this.view = view;
    }

    @Override
    public void onViewCreated() {
        if (initialListLoaded) {
            view.showList();
            return;
        }
        loadMovies();
    }


    public void loadMovies() {
        if (totalPages == 0 || currentPage <= totalPages) {
            movieService.getUpComingMovies(currentPage, this);
        }
    }

    @Override
    public void onResultReceived(UpcomingMovies body) {
        initialListLoaded = true;
        currentPage++;
        totalPages = body.getTotalPages();
        view.updateMovieList(body.getMovieList());
    }

    @Override
    public void onErrorReceived(Throwable throwable) {
        view.displayError(throwable);
    }
}
