package com.shahzad.movielist.presenter;

import com.shahzad.movielist.model.MovieDetails;
import com.shahzad.movielist.network.rest.MovieDetailsListener;
import com.shahzad.movielist.network.rest.MovieService;
import com.shahzad.movielist.view.IMovieDetailsView;

public class MovieDetailsPresenter extends BaseFragmentPresenter<IMovieDetailsView> implements MovieDetailsListener {
    private MovieService movieService;
    private IMovieDetailsView view;
    private MovieDetails movieDetails;


    public MovieDetailsPresenter(MovieService movieService) {
        this.movieService = movieService;
    }

    @Override
    public void setView(IMovieDetailsView view) {
        this.view = view;
    }

    @Override
    public void onViewCreated() {
    }

    public void loadDetails(int movieId) {
        if (movieDetails != null) {
            view.updateFragment(movieDetails);
            return;
        }
        movieService.getMovieDetails(movieId, this);
    }

    @Override
    public void onErrorReceived(Throwable throwable) {

    }

    @Override
    public void onResultReceived(MovieDetails details) {
        movieDetails = details;
        view.updateFragment(details);
    }
}
