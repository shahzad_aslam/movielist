package com.shahzad.movielist.view;

import com.shahzad.movielist.model.MovieDetails;

public interface IMovieDetailsView {
    void updateFragment(MovieDetails movieDetails);
}
