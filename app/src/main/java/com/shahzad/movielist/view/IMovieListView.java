package com.shahzad.movielist.view;

import com.shahzad.movielist.model.Movie;

import java.util.List;

public interface IMovieListView {
    void updateMovieList(List<Movie> movieList);

    void displayError(Throwable throwable);

    void showList();
}
