package com.shahzad.movielist.view.activity;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.shahzad.movielist.MovieApplication;
import com.shahzad.movielist.navigator.Navigator;
import com.shahzad.movielist.view.fragment.BaseFragment;

public class BaseActivity extends AppCompatActivity {
    public void replaceFragment(@IdRes int id, final BaseFragment fragment, @NonNull final String tag) {
        getSupportFragmentManager().beginTransaction().replace(id, fragment, tag).commit();
    }

    protected Navigator getNavigator() {
        return ((MovieApplication) getApplication()).getNavigator();
    }

}