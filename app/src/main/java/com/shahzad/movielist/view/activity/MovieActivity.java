package com.shahzad.movielist.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.shahzad.movielist.R;
import com.shahzad.movielist.view.fragment.MovieListFragment;
import com.shahzad.movielist.view.fragment.MovieListFragment.OnInteractionListener;

public class MovieActivity extends BaseActivity implements OnInteractionListener {

    public static final String MOVIE_LIST_FRAGMENT_TAG = MovieListFragment.class.getSimpleName();

    public static Intent getCallerIntent(Context context) {
        return new Intent(context, MovieActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        if (savedInstanceState == null) {
            replaceFragment(R.id.container, MovieListFragment.newInstance(), MOVIE_LIST_FRAGMENT_TAG);
        }
    }

    @Override
    public void onMovieItemClicked(int movieId) {
        getNavigator().navigateToMovieDetailsActivity(this, movieId);
    }
}
