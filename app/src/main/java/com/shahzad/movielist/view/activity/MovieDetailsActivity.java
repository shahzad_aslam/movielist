package com.shahzad.movielist.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.shahzad.movielist.R;
import com.shahzad.movielist.view.fragment.MovieDetailsFragment;
import com.shahzad.movielist.view.fragment.MovieDetailsFragment.OnInteractionListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieDetailsActivity extends BaseActivity implements OnInteractionListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private static final String MOVIE_ID = "MovieId";
    private static final String MOVIE_DETAILS_FRAGMENT_TAG = MovieDetailsFragment.class.getSimpleName();

    public static Intent getCallerIntent(Context context, int movieId) {
        return new Intent(context, MovieDetailsActivity.class).putExtra(MOVIE_ID, movieId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        ButterKnife.bind(this);
        setUpToolbar();
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            int movieId = 0;
            if (extras != null) {
                movieId = extras.getInt(MOVIE_ID);
            }
            replaceFragment(R.id.container, MovieDetailsFragment.newInstance(movieId), MOVIE_DETAILS_FRAGMENT_TAG);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void setTitle(String movieName) {
        toolbar.setTitle(movieName);
    }

    private void setUpToolbar() {
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
