package com.shahzad.movielist.view.adapter;

import com.shahzad.movielist.model.Movie;

import java.util.Comparator;

class DateComparator implements Comparator<Movie> {
    @Override
    public int compare(Movie with, Movie to) {
        return with.getReleaseDate().compareTo(to.getReleaseDate());
    }
}