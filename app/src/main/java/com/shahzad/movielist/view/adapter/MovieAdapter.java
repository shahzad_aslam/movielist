package com.shahzad.movielist.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shahzad.movielist.R;
import com.shahzad.movielist.model.Movie;
import com.shahzad.movielist.view.component.PosterView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieEntryHolder> {

    private List<Movie> movieList;
    private final OnMovieClickListener listener;

    public MovieAdapter(@NonNull OnMovieClickListener listener) {
        this.listener = listener;
        movieList = new ArrayList<>();
    }

    public void addMovies(List<Movie> movies) {
        if (movies != null) {
            movieList.addAll(movies);
        }
        notifyDataSetChanged();
    }

    @Override
    public MovieEntryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_movie, parent, false);
        return new MovieEntryHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MovieEntryHolder holder, int position) {
        final Movie movie = movieList.get(position);
        holder.getPosterImageView().setPoster(movie.getPosterPath());
        holder.getTitleTextView().setText(movie.getOriginalTitle());
        holder.getReleaseDateTextView().setText(getFormattedDate(movie.getReleaseDate()));
        holder.getVoteAverageTextView().setText(Float.toString(movie.getVoteAverage()));
        holder.getVoteCountTextView().setText(Integer.toString(movie.getVoteCount()));
        holder.getRow().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(movie.getId());
            }
        });
    }

    private String getFormattedDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(date);
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public void sortItemsByDate() {
        Collections.sort(movieList, new DateComparator());
        notifyDataSetChanged();
    }

    static class MovieEntryHolder extends RecyclerView.ViewHolder {
        private TextView titleTextView;
        private TextView releaseDateTextView;
        private TextView voteAverageTextView;
        private TextView voteCountTextView;
        private PosterView posterImageView;
        private View row;

        MovieEntryHolder(View itemView) {
            super(itemView);
            row = itemView;
            posterImageView = (PosterView) itemView.findViewById(R.id.posterView);
            titleTextView = (TextView) itemView.findViewById(R.id.textViewTitle);
            releaseDateTextView = (TextView) itemView.findViewById(R.id.textViewReleaseDate);
            voteAverageTextView = (TextView) itemView.findViewById(R.id.textViewVoteAverage);
            voteCountTextView = (TextView) itemView.findViewById(R.id.textViewVoteCount);
        }

        TextView getTitleTextView() {
            return titleTextView;
        }

        PosterView getPosterImageView() {
            return posterImageView;
        }

        TextView getReleaseDateTextView() {
            return releaseDateTextView;
        }

        TextView getVoteAverageTextView() {
            return voteAverageTextView;
        }

        TextView getVoteCountTextView() {
            return voteCountTextView;
        }

        View getRow() {
            return row;
        }
    }

    public interface OnMovieClickListener {
        void onClick(int movieId);
    }
}
