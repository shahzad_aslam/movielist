package com.shahzad.movielist.view.component;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;

import com.squareup.picasso.Picasso;

public class PosterView extends android.support.v7.widget.AppCompatImageView {

    private static final String BASE_URL = "https://image.tmdb.org/t/p/w300";

    public PosterView(@NonNull Context context) {
        super(context);
    }

    public PosterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PosterView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    public void setPoster(final String posterPath) {
        Picasso.with(getContext())
                .load(BASE_URL + posterPath)
                .into(this);

    }
}
