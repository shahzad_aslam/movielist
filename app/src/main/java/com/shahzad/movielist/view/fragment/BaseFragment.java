package com.shahzad.movielist.view.fragment;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.shahzad.movielist.MovieApplication;
import com.shahzad.movielist.network.rest.MovieService;
import com.shahzad.movielist.presenter.BaseFragmentPresenter;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment<P extends BaseFragmentPresenter> extends Fragment {
    protected P presenter;
    Unbinder unbinder;

    protected abstract P newPresenter();

    @Override
    @CallSuper
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = newPresenter();
    }

    @Override
    @CallSuper
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        if (presenter != null) {
            presenter.setView(this);
            presenter.onViewCreated();
        }
    }

    private MovieApplication getApplication() {
        return (MovieApplication) getActivity().getApplication();
    }

    MovieService getMovieService() {
        return getApplication().getMovieService();
    }

    @Override
    @CallSuper
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
