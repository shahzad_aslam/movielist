package com.shahzad.movielist.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shahzad.movielist.R;
import com.shahzad.movielist.model.MovieDetails;
import com.shahzad.movielist.presenter.MovieDetailsPresenter;
import com.shahzad.movielist.view.IMovieDetailsView;

import butterknife.BindView;

public class MovieDetailsFragment extends BaseFragment<MovieDetailsPresenter> implements IMovieDetailsView {

    @BindView(R.id.layout_progress)
    View progressLayout;
    @BindView(R.id.layout_movie_details)
    View movieDetailsLayout;
    @BindView(R.id.textViewBudget)
    TextView budgetTextView;
    @BindView(R.id.textViewOverview)
    TextView overviewText;

    public static final String MOVIE_ID = "movieId";
    private OnInteractionListener listener;

    @Override
    protected MovieDetailsPresenter newPresenter() {
        return new MovieDetailsPresenter(getMovieService());
    }

    public static BaseFragment newInstance(int movieid) {
        MovieDetailsFragment fragment = new MovieDetailsFragment();
        Bundle args = new Bundle();
        args.putInt(MOVIE_ID, movieid);
        fragment.setArguments(args);
        return fragment;
    }

    public MovieDetailsFragment() {
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_movie_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.loadDetails(getArguments().getInt(MOVIE_ID));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnInteractionListener) {
            listener = (OnInteractionListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement OnInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void updateFragment(MovieDetails movieDetails) {
        overviewText.setText(movieDetails.getOverview());
        budgetTextView.setText(Long.toString(movieDetails.getBudget()));
        progressLayout.setVisibility(View.INVISIBLE);
        movieDetailsLayout.setVisibility(View.VISIBLE);
        listener.setTitle(movieDetails.getOriginalTitle());
    }

    public interface OnInteractionListener {
        void setTitle(String movieName);
    }
}
