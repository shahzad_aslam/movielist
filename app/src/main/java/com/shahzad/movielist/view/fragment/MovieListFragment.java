package com.shahzad.movielist.view.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.shahzad.movielist.R;
import com.shahzad.movielist.model.Movie;
import com.shahzad.movielist.presenter.ListFragmentPresenter;
import com.shahzad.movielist.view.IMovieListView;
import com.shahzad.movielist.view.adapter.MovieAdapter;

import java.util.List;

import butterknife.BindView;

import static com.shahzad.movielist.view.adapter.MovieAdapter.OnMovieClickListener;

public class MovieListFragment extends BaseFragment<ListFragmentPresenter> implements IMovieListView, OnMovieClickListener {

    @BindView(R.id.layout_progress)
    View progressLayout;
    @BindView(R.id.layout_movie_list)
    View listLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private MovieAdapter adapter;
    private OnInteractionListener listener;


    public static MovieListFragment newInstance() {
        return new MovieListFragment();
    }

    public MovieListFragment() {
        setRetainInstance(true);
    }

    @Override
    protected ListFragmentPresenter newPresenter() {
        return new ListFragmentPresenter(getMovieService());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        newAdapter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_movie_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_movie_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sort_by_date:
                adapter.sortItemsByDate();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnInteractionListener) {
            listener = (OnInteractionListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement OnInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    private void newAdapter() {
        adapter = new MovieAdapter(this);
    }

    @Override
    public void updateMovieList(List<Movie> movieList) {
        showList();
        adapter.addMovies(movieList);
    }

    @Override
    public void displayError(Throwable throwable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.list_fetching_error)
                .setMessage(throwable.getMessage())
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        presenter.loadMovies();
                        dialogInterface.dismiss();
                    }
                }).create().show();
    }

    @Override
    public void showList() {
        progressLayout.setVisibility(View.INVISIBLE);
        listLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(int movieId) {
        listener.onMovieItemClicked(movieId);
    }

    public interface OnInteractionListener {
        void onMovieItemClicked(int movieId);
    }
}
