package com.shahzad.movielist.presenter;

import com.shahzad.movielist.model.MovieDetails;
import com.shahzad.movielist.network.rest.MovieDetailsListener;
import com.shahzad.movielist.network.rest.MovieService;
import com.shahzad.movielist.view.IMovieDetailsView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MovieDetailsPresenterTest {
    @Mock
    IMovieDetailsView view;
    @Mock
    MovieService movieService;

    MovieDetailsPresenter presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new MovieDetailsPresenter(movieService);
        presenter.setView(view);
    }

    @Test
    public void loadDetails() {
        final int movieId = 1;
        presenter.loadDetails(movieId);
        verify(movieService).getMovieDetails(eq(movieId), any(MovieDetailsListener.class));
    }

    @Test
    public void onResultReceived() {
        final int movieId = 1;
        MovieDetails details = new MovieDetails();
        presenter.loadDetails(movieId);
        ArgumentCaptor<MovieDetailsListener> captor = ArgumentCaptor.forClass(MovieDetailsListener.class);

        verify(movieService).getMovieDetails(eq(movieId), captor.capture());
        captor.getValue().onResultReceived(details);

        verify(view).updateFragment(details);
    }
}